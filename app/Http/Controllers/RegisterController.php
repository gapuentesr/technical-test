<?php

namespace App\Http\Controllers;

use App\Models\Register;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function store(Request $request){

        $validated = $request->validate([
            'name' => ['required'],
            'lastname' => ['required'],
            'document_type' => ['required'],
            'document_number' => ['required', 'min:5'],
            'telephone' => ['required', 'min:7'],
        ]);

        Register::create($validated);

        session()->flash('status', 'Registro realizado correctamente');

        return to_route('Inicio');
    }
}
