<?php

return [
    'required' => 'El campo :attribute es obligatorio',
    'min' => [
        'array' => 'El campo :attribute debe tener más de :min caracteres',
        'file' => 'El campo :attribute debe tener más de :min caracteres',
        'numeric' => 'El campo :attribute debe tener más de :min caracteres',
        'string' => 'El campo :attribute debe tener más de :min caracteres',
    ],

    'attributes' => [
        'name' => 'nombre',
        'lastname' => 'apellido',
        'document_type' => 'tipo de documento',
        'document_number' => 'número de documento',
        'telephone' => 'teléfono',
    ],
];
